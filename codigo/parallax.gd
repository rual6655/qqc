extends ParallaxLayer

#funcion que utiliza la propiedad motion_offset en el eje x para realizar un movimiento horizontal del fondo.
#multipicar delta por x para conseguir el efecto deseado
func _process(delta):
	motion_offset.x -= 100 * delta
