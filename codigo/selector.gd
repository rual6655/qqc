extends Node2D

onready var pop = preload("res://escenas/Panel.tscn")

func _ready():
	$Logo/AnimationPlayer.stop()
	var hijos = $cuadro1.get_children()
	hijos += $cuadro2.get_children()
	for hijo in hijos:
		if hijo is Button:
			hijo.connect("equipo_elegido",self,"on_equipo_elegido",[hijo])

func _on_masequipos_pressed():
	GLOBAL.sonido_boton()
	var ubicacion = $cuadro1.rect_position.x + $cuadro2.rect_position.x
	
	if ubicacion >0:
		$anim.play("desplazar")
	else:
		$anim.play_backwards("desplazar")

func on_equipo_elegido(boton):
	var elegido = pop.instance()
	#coloca el pivot en el centro del boton pulsado 
	elegido.rect_pivot_offset = boton.rect_global_position + Vector2(boton.rect_size.x/2,boton.rect_size.y/2)
	elegido.get_node("equipo").nombreEquipo = boton.nombreEquipo
	elegido.get_node("equipo").escudoEquipo = boton.escudoEquipo
	elegido.get_node("equipo").fichero = boton.fichero
	elegido.get_node("equipo").aciertos = boton.aciertos
	add_child(elegido)
	GLOBAL.nivel_seleccionado = boton.fichero
	

func _on_atras_pressed():
	GLOBAL.sonido_boton()
	transicion.ir_a_escena("res://escenas/portada.tscn")
