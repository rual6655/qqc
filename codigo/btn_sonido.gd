extends Button

func _onready():
	actualiza_icono()

func actualiza_icono():
	if GLOBAL.conSonido:
		$icono.frame = 0
	else:
		$icono.frame = 1
		
	GLOBAL.activa_musica()

func _on_btn_sonido_pressed():
	GLOBAL.conSonido = !GLOBAL.conSonido
	GLOBAL.config_musica["quieroSonido"] = GLOBAL.conSonido
	GLOBAL.grabar_configuracion()
	actualiza_icono()
