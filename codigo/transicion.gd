extends CanvasLayer

var siguiente_escena

func ir_a_escena(escena):
	siguiente_escena = escena
	$anim.play("fade")
	
func cambio_de_escena():
	if siguiente_escena != null:
		get_tree().change_scene(siguiente_escena)
