extends Node2D

# cargamos nuestro fichero de pregutnas en una variable
onready var preguntas_del_nivel:Dictionary = GLOBAL.cargar_datos_preguntas(GLOBAL.nivel_seleccionado)

#variables

var numero_pregutnas_nivel = 10
var aciertos
var turno
var respuesta_correcta = ""
var puedoResponder = false

#arrays 
var indices:Array = [] #aqui almacenamos todos los numeros de pregunta, la key del diccionario
var verificadores:Array = [] #en este almacenamos las casillas de check

func _ready():
	if preguntas_del_nivel.empty():
		salir()
	else:
		inicializar()
	
func inicializar():
	var numero_preguntas_disponibles = contar_indices()
	
	#comprobamos que tenemos suficientes pregutnas para el juego
	if numero_preguntas_disponibles < numero_pregutnas_nivel:
		salir()
		
	aciertos = 0
	turno = 0
	
	#conectamos los botones de respuesta con nuestro codigo
	conectar_botones()
	
	#capturamos las casillas de check en un array
	captura_casillas()
	
	randomize() #actualiza la semilla para generar aleatorios
	indices.shuffle() #barajea aleatoriamiente el array de indices
	
	#cargamos la pregutnal
	carga_pregunta()
	
	
func carga_pregunta():
	#asigna la pregutna alamacenada en el indice correspondiente al turno en el juego
	var pregunta_actual = elige_pregunta(indices[turno])
	
	#aleatorios los botones
	var botones_random = $respuestas.get_children()
	randomize()#actualiza la semilla para generar aleatorios
	botones_random.shuffle()#barajea aleatoriamiente el array de indices
	
	#actualicamos los nodos con los datos asignados
	$panel_pregunta/texto_pregunta.text = pregunta_actual["pregunta"]
	
	botones_random[0].get_node("texto_respuesta").text = pregunta_actual["respA"]
	botones_random[0].text = "A"
	botones_random[1].get_node("texto_respuesta").text = pregunta_actual["respB"]
	botones_random[1].text = "B"
	botones_random[2].get_node("texto_respuesta").text = pregunta_actual["respC"]
	botones_random[2].text = "C"
	botones_random[3].get_node("texto_respuesta").text = pregunta_actual["respD"]
	botones_random[3].text = "D"
	
#	$respuestas/boton_respuesta1/texto_respuesta.text = pregunta_actual["respA"]
#	$respuestas/boton_respuesta2/texto_respuesta.text = pregunta_actual["respB"]
#	$respuestas/boton_respuesta3/texto_respuesta.text = pregunta_actual["respC"]
#	$respuestas/boton_respuesta4/texto_respuesta.text = pregunta_actual["respD"]
	
	#asignamos la respuesta correcta
	respuesta_correcta = pregunta_actual["buena"]
	# activamos la posibilidad de responder
	puedoResponder = true
	

func al_responder(boton):
	#comprobamos si podemos responder
	if !puedoResponder: return
	#desactiva la posobilidad de responder hasta que carga la siguiente pregunta
	puedoResponder = false
	
	#sonido
	if GLOBAL.conSonido : $sfx_respuesta.play()
	
	#comprobamos si es el boton correcto
	if boton.text == respuesta_correcta:
		boton.respuesta_correcta()
		aciertos += 1
		cambia_estado_casilla_check(verificadores[turno], 1)
		
	else:
		boton.respuesta_erronea()
		cambia_estado_casilla_check(verificadores[turno], 2)
		
	#creamos un temporizador para ver el efecto del boton
	yield(get_tree().create_timer(0.5), "timeout")
	
	
	#comprobamos si es el turno final
	if turno < numero_pregutnas_nivel-1:
		turno += 1 #pasamos a otra pregunta
		transicion.ir_a_escena("")
		carga_pregunta()
	else:
		#actualizamos el marcador
		GLOBAL.marcador = aciertos
		#no grabamos aqui, lo hacemos en la pantalla de puntuación
		# y solo si es mejor a la anterior puntuación
		transicion.ir_a_escena("res://escenas/premios.tscn")

func elige_pregunta(id_pregunta:String):
	#comprobamos si el indice existe
	if preguntas_del_nivel.has(id_pregunta):
		return preguntas_del_nivel[id_pregunta] # si existe devuelve el diccionario que almacena
	else:
		return {} #si no existe devuelve vacio
	
func captura_casillas():
	var casillas = $semaforo.get_children() #capturamos los hijos del nodo semaforo
	
	#limpiamos el array
	verificadores.clear()
	
	for casilla in casillas:
		#añadimos cada casilla al array
		verificadores.append(casilla)
		#inicializamos el frame de la casilla
		cambia_estado_casilla_check(casilla,0) # 0 = blanco , 1 = acierto , 2 = fallo

func cambia_estado_casilla_check(casilla, num_frame):
	#capturamos el sprite donde cambiaremos el frame
	casilla.get_node("Sprite").frame = num_frame
	
func conectar_botones():
	var botones = $respuestas.get_children() #captura los hijos del nodo respuestas
	for boton in botones:
		if boton is Button: #comprueba que cada hijo es un boton
			#conectamos la señal PRESSED con la funcion AL_RESPONDER y pasamos el boton como parametro
			boton.connect("pressed",self,"al_responder",[boton])

func contar_indices():
	#cargamos en el array indices todos los numeros de pregunta
	indices = preguntas_del_nivel.keys()
	return indices.size() #devolvemos el tamaño del array

func salir():
	transicion.ir_a_escena("res://escenas/portada.tscn")
