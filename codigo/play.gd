extends Node2D

onready var padre = get_parent()

func _on_Button_pressed():
	GLOBAL.sonido_boton()
	if padre.name == "portada":
		transicion.ir_a_escena("res://escenas/selector.tscn")
	else:
		transicion.ir_a_escena("res://escenas/juego.tscn")
