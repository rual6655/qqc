extends Node2D

const RUTA_PREGUNTAS = "res://datos/"
const RUTA_DATOS = "user://"

var nivel_seleccionado = "Nivel"

# puntuaciones
var puntuaciones:Dictionary
var puntuacionCambiada
var marcador
var record

#sonido
onready var botonSFX = $sfx_boton
var conSonido = false
var config_musica:Dictionary = {}

func _ready():
	if comprueba_existencia("configuracion"):
		config_musica = cargar_datos("configuracion")
		conSonido = config_musica["quieroSonido"]
	else:
		config_musica = {"quieroSonido":false}
		grabar_configuracion()
	
	if comprueba_existencia("marcadores"):
		puntuaciones = cargar_datos("marcadores")
		# si el archivo contiene la puntuación correcta no necesita grabarse
		puntuacionCambiada = false
	else:
		puntuaciones = {}
		# si el archivo ha sido modificado necesita grabarse
		puntuacionCambiada = true
		grabar_puntuacion()
		
func grabar_configuracion():
	var archivo = File.new()
	var ruta_archivo = RUTA_DATOS + "configuracion.json"
	archivo.open(ruta_archivo, File.WRITE) #abrir archivo para su escritura
	#almacena el diccionario en un archivo como una linea json
	archivo.store_line(to_json(config_musica))
	archivo.close()
	

func activa_musica():
	if conSonido && !$musica.playing:
		$musica.play()
	if !conSonido:
		$musica.stop()

func sonido_boton():
	if conSonido : botonSFX.play()
		
func grabar_puntuacion():
	if !puntuacionCambiada: return 
	var archivo = File.new()
	var ruta_archivo = RUTA_DATOS + "marcadores.json"
	archivo.open(ruta_archivo, File.WRITE) #abrir archivo para su escritura
	#almacena el diccionario en un archivo como una linea json
	archivo.store_line(to_json(puntuaciones))
	archivo.close()
	# si el archivo contiene la puntuación correcta no necesita grabarse
	puntuacionCambiada = false
		


func comprueba_existencia(nombre_archivo):
	#creamos una variable para manejar el archivo
	var archivo = File.new()
	
	#completamos la ruta de nuestro fichero
	var ruta_archivo = RUTA_DATOS + nombre_archivo + ".json"
	
	#devuelve un diccionario si el archivo existe
	return archivo.file_exists(ruta_archivo)
	
func comprueba_existencia_preguntas(nombre_archivo):
	#creamos una variable para manejar el archivo
	var archivo = File.new()
	
	#completamos la ruta de nuestro fichero
	var ruta_archivo = RUTA_PREGUNTAS + nombre_archivo + ".json"
	
	#devuelve un diccionario si el archivo existe
	return archivo.file_exists(ruta_archivo)

func cargar_datos(nombre_archivo) -> Dictionary:
	#comprobamos que llega un nombre
	if nombre_archivo == null: return {}
	
	#creamos una variable para manejar el archivo
	var archivo = File.new()
	
	#completamos la ruta de nuestro fichero
	var ruta_archivo = RUTA_DATOS + nombre_archivo + ".json"
	
	#comprueba si el archivo existe, si no devuelvo un diccionario vacio
	if !archivo.file_exists(ruta_archivo): return {}
	
	#el archivo existe
	#abrimos el archivo para su lectura
	archivo.open(ruta_archivo, File.READ)
	
	#asignamos el contenido a un archivo tipo Dictionary
	var datos:Dictionary = {}
	datos = parse_json(archivo.get_as_text())
	
	#cerramos el archivo y regresamos el diccionario
	archivo.close()
	
	return datos
	
func cargar_datos_preguntas(nombre_archivo) -> Dictionary:
	#comprobamos que llega un nombre
	if nombre_archivo == null: return {}
	
	#creamos una variable para manejar el archivo
	var archivo = File.new()
	
	#completamos la ruta de nuestro fichero
	var ruta_archivo = RUTA_PREGUNTAS + nombre_archivo + ".json"
	
	#comprueba si el archivo existe, si no devuelvo un diccionario vacio
	if !archivo.file_exists(ruta_archivo): return {}
	
	#el archivo existe
	#abrimos el archivo para su lectura
	archivo.open(ruta_archivo, File.READ)
	
	#asignamos el contenido a un archivo tipo Dictionary
	var datos:Dictionary = {}
	datos = parse_json(archivo.get_as_text())
	
	#cerramos el archivo y regresamos el diccionario
	archivo.close()
	
	return datos
