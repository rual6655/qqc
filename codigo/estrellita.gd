extends TextureRect

const COLOR_ON = "ffffff"

export var movilidad = 50

func _ready():
	randomize()
	$Timer.wait_time = int (rand_range(4,8))
	$Timer.start()
	
	

func _on_Timer_timeout():
	if modulate !=  Color(COLOR_ON): return
	if $anim.is_playing(): return
	randomize()
	if randi()%100 > movilidad:
		$Timer.stop()
		$anim.play("rotar")
	
	



func _on_anim_animation_finished(anim_name):
	$Timer.start()
