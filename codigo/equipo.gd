extends Button

export var escudoEquipo:Texture
export var nombreEquipo:String
export var aciertos:int = 0
export var fichero:String = "Nivel 1"

const COLOR_ON = "ffffff"
const COLOR_OFF = "723723"

signal equipo_elegido

func _ready():
	if existe_fichero():
		cargar_marcadores()
		dibuja_equipo()
	else:
		#ocultamos el boton si el fichero no existe
		visible = false
	
func cargar_marcadores():
	#comprobamos si existe la key cargada en el fichero dentro de puntuaciones
	if !GLOBAL.puntuaciones.has(fichero):
		#si no existe, la añadimos
		GLOBAL.puntuaciones[fichero] = 0
		#marcamos que la puntuacion ha sido cambiada
		GLOBAL.puntuacionCambiada = true
		GLOBAL.grabar_puntuacion()
	
	# antes de salir actualizamos el valor de aciertos
	aciertos = GLOBAL.puntuaciones[fichero]
	

func existe_fichero():
	return GLOBAL.comprueba_existencia_preguntas(fichero)

func dibuja_equipo():
	if escudoEquipo != null:
		$graficos/escudo.texture = escudoEquipo
		
	if aciertos > 0:
		var amarillas = floor(aciertos/3)
		for estrella in amarillas:
			var iluminar = get_node("graficos/centrado/marcador/estrellita"+str(estrella+1))
			iluminar.modulate = Color(COLOR_ON)
			
	if nombreEquipo == null or nombreEquipo=="":
		$nombre.text = "Sin asignar"
	else:
		$nombre.text = nombreEquipo

func _on_equipo_pressed():
	GLOBAL.sonido_boton()
	emit_signal("equipo_elegido")
