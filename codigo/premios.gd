extends Node2D

var aciertos
var record

func _ready():
	$Logo.get_node("AnimationPlayer").stop()
	comprobar_estrellas()
	comprobar_record()
	cambiar_textos()
	
func cambiar_textos():
	$Panel/texto_aciertos.text = "Has acertado "+str(aciertos)+" de 10 preguntas"
	
func comprobar_record():
	record = GLOBAL.puntuaciones[GLOBAL.nivel_seleccionado]
	if aciertos > record:
		$Panel/nuevorecord.visible = true
		$anim.play("record")
		#grabamos el nuevo record
		GLOBAL.puntuaciones[GLOBAL.nivel_seleccionado] = aciertos
		GLOBAL.puntuacionCambiada = true
		GLOBAL.grabar_puntuacion()
		
	else:
		$Panel/nuevorecord.visible = false
		

func comprobar_estrellas():
	aciertos = GLOBAL.marcador
	var estrellas = floor(aciertos/3)
	enciende(estrellas)
	
func enciende(estrellas):
	for i in range(3): #recorre las estrellas disponibles y cambia su estado visible el correspondiente
		var contador = i + 1
		var estrella = get_node("Panel/estrellas/estrella"+str(contador)+"/on")
		if contador <= estrellas:
			estrella.visible = true
		else:
			estrella.visible = false

func _on_btn_a_selector_pressed():
	GLOBAL.sonido_boton()
	transicion.ir_a_escena("res://escenas/selector.tscn")


func _on_btn_a_nivel_pressed():
	GLOBAL.sonido_boton()
	transicion.ir_a_escena("res://escenas/juego.tscn")
